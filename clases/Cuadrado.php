<?php


class Cuadrado {
   public $lado=0;
   
   public function area(){
       return $this->lado*$this->lado;
   }
   
   public function perimetro(){
       return 4*$this->lado;
   }
}
