<?php

class Operaciones {
    /*
     * dos propiedades publicas 
     * llamadas numero1 y numero2
     */
    
    public $numero1=0;
    
    public $numero2=0;
    
    // metodo sumar
    public function sumar(){
        return $this->numero1 + $this->numero2;
    }
    
    // metodo estatico
    public static function sumarEstatico($a,$b){
        return $a+$b;
    }
           
}
