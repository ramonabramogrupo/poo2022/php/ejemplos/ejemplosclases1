<?php

class Persona {
    
    // propiedad
    public $altura=0;
    
    // propiedad
    public $nombre="";
    
    // metodo
    public function hablar(){
        return $this->nombre . " bla bla bla";
    }
    
    // metodo estatico
    public static function presentar(){
        // en un metodo estatico no puedo hacer
        // referencia a elementos con $this
        return "hola";
    }
}
